import torch
import torchvision
from torch import nn
from PIL import Image
import json
from torchvision import transforms
import custom_logger

class NaturifyModelInferencer:
    def __init__(self):
        self.logger = custom_logger.Logger("NaturifyModelInferencer")
        self.logger.verbose = True

        naturify_state_dict = torch.load("/models/naturify_state_dict.pth", map_location=torch.device('cpu'))

        self.naturify_model = torchvision.models.vit_b_16()
        self.naturify_model.heads = nn.Sequential(
            nn.Linear(in_features=self.naturify_model.heads[0].in_features, out_features = 10000)
        )
        self.naturify_model.load_state_dict(naturify_state_dict)
        self.naturify_model.eval()
        # I don't know if this is nescessary
        del naturify_state_dict
        print("loaded Naturify model")

        # Load the categories
        with open("/models/categories.json","r") as f:
            self.categories = json.load(f)

        # transform input into the Naturify model
        self.transform = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])

    # inference the Naturify model on an image, and return a list of dictionary structures containing predictions for what species the image contains
    def inference(self, image: Image):
        try:
            #image = torch.tensor(image)
            self.logger.log("loading image...")
            image = self.transform(image)

            # unsqueeze the image tensor so that it is compatible with the model
            image = image.unsqueeze(0)

        except Exception as e:
            self.logger.log("failed to transform tensor from image bytes")
        

        # inference the model (output logits)
        self.logger.log("inferencing model on processed image...")
        self.logger.log(f"image tensor shape: {image.shape}")
        out = self.naturify_model(image)

        # use softmax to convert the logits into probablities
        out = torch.softmax(out, dim=1)
        
        # find the top 5 most certain classes
        vals, idxs = torch.topk(out, 5, dim=1)

        # create the list of dictionary structures that contain the predictions
        predictions = list()
        for idx in idxs.squeeze():
            predictions.append({
                "species" : self.categories[idx],
                "score" : out.squeeze()[idx].item(),
            })
        return predictions

if __name__ == "__main__":
    model = NaturifyModelInferencer("/models/naturify_state_dict.pth")


    # run an HTTP service for the app
    from fastapi import FastAPI, File, UploadFile, Response
    from fastapi.responses import JSONResponse, HTMLResponse

    app = FastAPI()

    # search an image 
    @app.post("/classify")
    async def search_image(file: UploadFile = File(...)):
        # read the image data from the HTTP request and open it
        image = await file.read()
        pil_image = Image.open(io.BytesIO(image))
        results = model.inference(pil_image)
        return JSONResponse({
            "result" : results
        })
    
    import uvicorn
    uvicorn.run(app, host='0.0.0.0', port=80)