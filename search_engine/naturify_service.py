'''
This creates a gRPC server that hosts the naturify classification model
'''

import grpc 
import classification_pb2
import classification_pb2_grpc
import index_network_config

from PIL import Image
import io
import naturify

from concurrent import futures
import custom_logger

class NaturifyClassificationService(classification_pb2_grpc.ClassificationServicer):
    def __init__(self):
        self.logger = custom_logger.Logger("NaturifyClassificationService")
        self.logger.verbose = True


        self.inferencer = naturify.NaturifyModelInferencer()

    
    def ClassifyImage(self, request, context):
        self.logger.log("recieved classification request")
        

        image_bytes = request.image_bytes

        
        # parse the bytes for the image into a Pillow Image
        image = Image.open(io.BytesIO(image_bytes))

        self.logger.log("opened image bytes")

        classification_results = self.inferencer.inference(image)

        self.logger.log("recieved classification results from model")

        results_proto_list = [
            classification_pb2.ClassificationResult(
                class_name=result['species']['name'],
                class_id=result['species']['id'],
                class_metadata=str(result['species']),
                confidence=result['score']
            )
        for result in classification_results]

        response_proto = classification_pb2.ClassificationResponse(
            results=results_proto_list
        )

        return response_proto
    

def serve():
    print("starting gRPC service")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers = 10))
    naturify_service = NaturifyClassificationService()
    print("created naturify service")
    classification_pb2_grpc.add_ClassificationServicer_to_server(naturify_service, server)

    port = index_network_config.naturify_classification_port
    
    print(f"adding port for service: {port}")
    server.add_insecure_port(f'[::]:{port}')

    try:
        print("starting server...")
        server.start()
        server.wait_for_termination()
    finally:
        print("finished service")


if __name__ == "__main__":
    # start the server to host the naturify classification model
    serve()