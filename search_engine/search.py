import vector_index_client
from PIL import Image
import embedding_provider
import vector_index
from typing import List
import page_index
import os
from transformers import pipeline


class SearchEngineClient:
    '''
    
        The functionality of the search engine
    
    '''
    def __init__(self, 
            vector_index_path,
            page_index_path,
            load_image_model = False
        ):

        self.vector_index_path = vector_index_path
        self.page_index_path = page_index_path

        # load the vector index clients
        self.open_clip_image_client = vector_index_client.VectorIndexClient("open_clip_image")

        self.snowflake_arctic_s_client = vector_index_client.VectorIndexClient("snowflake_arctic_s")

        # load the embedding providers
        if load_image_model:
            self.open_clip_embedding_provider = embedding_provider.EmbeddingProvider("open_clip", index_clients=False)

        
         
        self.snowflake_arctic_s_embedding_provider = embedding_provider.EmbeddingProvider("snowflake_arctic_s", index_clients=False)

        # get the path to the page index
        self.page_index_path = os.environ["PAGE_INDEX_PATH"]
        self.page_index_client = page_index.PageIndexClient(self.page_index_path)


        # initialize the question answering model
        # This is commmented out for now, because the QA model was being extrmely slow
        #self.qa_model = pipeline("question-answering")
    
    def search_image(self, image: Image) -> List[vector_index.SearchResult]:
        image_embedding = self.open_clip_embedding_provider.open_clip_embed_image(image)
        return self.open_clip_image_client.search(image_embedding)

    def search_text(self, text: str) -> List[vector_index.SearchResult]:

        # generate an embedding for the text, and search it in the vector index client
       
           
     
        text_embedding = self.snowflake_arctic_s_embedding_provider.embed_text(text)

        raw_text_results = self.snowflake_arctic_s_client.search(text_embedding)

    
        

        # map page url to result for the page
        search_result_dict = {}

        for result in raw_text_results:
            if not result.payload.page_url in search_result_dict.keys():
                print(result.payload.page_url)
                result_page_data = self.page_index_client.retrieve_page_data(result.payload.page_url)
                if result_page_data == None: 
                    print("recieved result outside of ranked index")
                    continue

                if len(result_page_data.image_urls) == 0:
                    thumbnail_url = ""
                else:
                    thumbnail_url = result_page_data.image_urls[0]
            
                search_result_dict[result.payload.page_url] = {
                    "text_sections" : [result.payload.text_section_idx],
                    "page_data" : result_page_data.dict(),
                    "thumbnails" : [thumbnail_url]
                }
            else:
                search_result_dict[result.payload.page_url]["text_sections"].append(result.payload.text_section_idx)
        
        return list(search_result_dict.values())
    

    def page_qa(self, page_url: str, question: str) -> dict:
        '''given a page URL, retrieve the content from the page, and inference the qa model on the question, with the page as the context'''

        page_data = self.page_index_client.retrieve_page_data(page_url)
        if page_data is None: 
            print(f"(page_aq) failed to load page data for url: {page_url}")
            return {"answer" : None}
        else:
            # create a string containing all of the content on the page
            context = " ".join(page_data.text_sections)

            # inference the model
            return self.qa_model(question = question, context = context)

    






        

if __name__ == "__main__":
    pass
