import grpc 
import classification_pb2
import classification_pb2_grpc
from typing import List
import index_network_config
import custom_logger
from io import BytesIO

class NaturifyClient:
    def __init__(self, hostname=None):
        self.logger = custom_logger.Logger(f"NaturifyClient")
        self.logger.verbose = True


        # the route to the gRPC service that is hosting the model
        port = index_network_config.naturify_classification_port
        service_route = f"naturify_service:{port}" if hostname is None else f"{hostname}:{port}"
        
        self.logger.log(f"initiating client for service at route: {service_route}")

        self.channel = grpc.insecure_channel(service_route)
        self.stub = classification_pb2_grpc.ClassificationStub(self.channel)


    def classify_image(self, image_bytes: bytes) -> List[dict]:
        # create the protocol object to request the classification service
        request_proto = classification_pb2.ImageClassificationRequest(
            image_bytes=image_bytes
        )
        # send the request to the classification service

        classification_response_proto = self.stub.ClassifyImage(request_proto)

        # we have an object of type: classification_pb2.ClassificationResponse
        # it needs to be converted to a list of result dictionaries

        response = [
            {
                "class_name" : result.class_name,
                "class_id" : result.class_id,
                "class_metadata" : result.class_metadata,
                "confidence" : result.confidence
            }
        for result in classification_response_proto.results]
        
        return response
        

if __name__ == "__main__":
    client = NaturifyClient(hostname="localhost")

    with open("/home/cameron/Search_Engine/flower.jpg", "rb") as f:
        image_bytes = f.read()
    
    response = client.classify_image(image_bytes)
    print(response)
