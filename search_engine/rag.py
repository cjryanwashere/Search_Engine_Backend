from transformers import AutoTokenizer, pipeline

model_name = "glaiveai/Llama-3-8B-RAG-v1"
tokenizer = AutoTokenizer.from_pretrained(model_name)

# Example user query
user_query = """Document:0

Title: Financial Compliance and Company Statements

Text: This document provides a detailed overview of the financial compliance measures and statements issued by corporations. It discusses various aspects of financial reporting, the importance of accurate financial disclosures, and the role of chief executive officers in certifying financial documents. The document elaborates on the legal frameworks that govern corporate financial disclosures, including the Securities Exchange Act of 1934, which mandates that public companies must provide periodic reports detailing their financial status. These reports are crucial for investors, regulators, and other stakeholders who rely on them to make informed decisions. The document also highlights the penalties for non-compliance with financial reporting requirements, which can include fines and criminal charges for executives who misreport financial information.

Document:1

Title: Certification of Financial Reports by CEO of Black Knight, Inc.

Text: Pursuant to 18 U.S.C. §1350, the Chief Executive Officer of Black Knight, Inc., a corporation registered in Delaware, has officially certified the periodic financial report containing the company's financial statements. The certification confirms that these reports comply fully with the requirements of Section 13(a) or 15(d) of the Securities Exchange Act of 1934. The CEO, Anthony M. Jabbour, asserts that the information contained within these reports fairly presents, in all material respects, the financial condition and results of operations of Black Knight, Inc. The certification is part of the official documents filed with the Securities and Exchange Commission and serves as a testament to the accuracy and fairness of the financial disclosures made by the company. This certification is crucial as it assures stakeholders of the reliability of the financial statements provided by Black Knight, Inc.

Answer Mode: Grounded

Question: How does the CEO of Black Knight, Inc. ensure compliance with the Securities Exchange Act of 1934, and what are the implications of the certification provided?"""

# Prepare chat template
chat = [
   {"role": "system", "content": "You are a conversational AI assistant that is provided a list of documents and a user query to answer based on information from the documents. The user also provides an answer mode which can be 'Grounded' or 'Mixed'. For answer mode Grounded only respond with exact facts from documents, for answer mode Mixed answer using facts from documents and your own knowledge. Cite all facts from the documents using <co: doc_id></co> tags."},
   {"role": "user", "content": user_query}
]

prompt = tokenizer.apply_chat_template(chat, tokenize=False, add_generation_prompt=True)

# Set up pipeline and generate response
pipe = pipeline('text-generation', model=model_name, tokenizer=model_name, device=0)
output = pipe(
    prompt, 
    max_length=1000, 
    num_return_sequences=1, 
    top_k=50, 
    top_p=0.95, 
    temperature=0.5, 
    do_sample=True, 
    return_full_text=False
)

print(output[0]['generated_text'])
