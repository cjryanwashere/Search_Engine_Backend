

function fillImageSearchResults(search_response) {
    clearSearchResults()
    console.log("displaying search results")
    resultList = document.getElementById("result-list");
    for (let i=0; i< search_response.length; i++) {
        result = search_response[i];
        resultList.appendChild(
            makeImageSearchResult(
                result.payload.image_url, 
                result.payload.page_url
            )
        )
    }   
}

function clearSearchResults() {
    console.log("emptying search result list")
    document.getElementById("result-list").innerHTML = "";
}

function makeImageSearchResult(image_url, page_url) {
    let resultContainer = document.createElement("li");
    resultContainer.setAttribute("class", "result-container");
    
    let resultElement = document.createElement("div");
    resultElement.setAttribute("class", "result-element")

    let resultImage = document.createElement("img");
    resultImage.src = image_url
    resultImage.setAttribute("class", "result-image")

    resultElement.appendChild(resultImage);

    let resultTitle = document.createElement("p");
    resultTitle.innerHTML="";
    resultTitle.setAttribute("class", "result-text")
    resultElement.appendChild(resultTitle);

    let resultLink = document.createElement("a")
    resultLink.href = page_url;
    resultLink.innerHTML = page_url
    resultElement.appendChild(resultLink)

    resultContainer.appendChild(resultElement);

    return resultContainer;
}

function makeTextSearchResult(thumbnail_url, page_url, text_sections) {

    let resultContainer = document.createElement("li");
    resultContainer.setAttribute("class", "result-container");

    let resultElement = document.createElement("div");
    resultElement.setAttribute("class", "result-element")

    if (thumbnail_url != "") {
        let resultImage = document.createElement("img");
        resultImage.src = thumbnail_url
        resultImage.setAttribute("class", "result-image")

        resultElement.appendChild(resultImage);
    }

    let resultTitle = document.createElement("p");
    resultTitle.setAttribute("class", "result-text")

    for (const sequence of text_sections) {
        resultTitle.innerHTML += `${sequence}<br><br>`;
    }

    resultElement.appendChild(resultTitle);

    let resultLink = document.createElement("a")
    resultLink.href = page_url;
    resultLink.innerHTML = page_url
    resultElement.appendChild(resultLink)

    resultContainer.appendChild(resultElement);

    // append an option to ask a question about the content of the specific page
    //resultContainer.appendChild(
    //    createPageQAOption(page_url)
    //);
    return resultContainer
}

function createPageQAOption(page_url) {
    // a string that identifies the page
    let page_string = page_url.split("/").reverse()[0];


    let qaElement = document.createElement("div");
    qaElement.setAttribute("class", "qa-container");
    qaElement.setAttribute("id", `qa-${page_string}`);


    let questionInput = document.createElement("input");
    questionInput.type = "text";
    questionInput.placeholder = "ask a question about this page";
    questionInput.setAttribute("class", "question-input");
    questionInput.setAttribute("id", `input-${page_string}`)
    qaElement.appendChild(questionInput);

    let askButton = document.createElement("button");
    let ask = document.createElement("p");
    ask.innerHTML = "Ask!";
    askButton.appendChild(ask);
    askButton.setAttribute("class", "ask-button");

    askButton.addEventListener("click", e => {
        console.log(`asking question: ${questionInput.value}`);
        fetch("/page_qa", {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                "page_url" : page_url,
                "question" : questionInput.value
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
        })
    });

    qaElement.appendChild(askButton);

    return qaElement
}


function imageDropHandler(e) {
    console.log("image file dropped");

    e.preventDefault();
    document.getElementById("image-input").files = e.dataTransfer.files;

    searchImage({
        target: document.getElementById("image-input")
    });

    
}


function imageDragOverHandler(e) {
    document.getElementById("image-drop-text").innerHTML = "Lets go!";
    e.preventDefault()
}

document.getElementById("image-input").addEventListener("change", searchImage)

function searchImage(e) {
    var inputElement = e.target;

    // Check if a file is selected
    if (inputElement.files.length > 0) {
        // Get the selected file
        var file = inputElement.files[0];

        // display the query image
        showQueryImage(file);

        // Create a FormData object
        var formData = new FormData();
        // Append the file to the FormData object
        formData.append('file', file);

        // CORS must be disabled for this to work
        //var apiEndpoint = 'http://192.168.1.70:8000/search_image';
        let apiEndpoint = "/search_image";

        // Example: Sending image data to the API using fetch
        fetch(apiEndpoint, {
            method: 'POST',
            body: formData,
        }).then(response => response.json())
        .then(data => {
            // Handle the API response
            console.log("recieved search api response: ");
            console.log(data);
            fillImageSearchResults(data.search_result);
        })
    } else {
        alert("Please select an image file.");
    }
}

function searchText() {
    let searchQuery = document.getElementById("text-input").value;

    //let apiRoute = "http://192.168.1.70:8000/search_text";
    let apiRoute = "/search_text";
    fetch(apiRoute, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            "query" : searchQuery
        })
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);
        fillTextSearchResults(data.search_result);
    })
}

function fillTextSearchResults(search_response) {

    clearSearchResults();
    resultList = document.getElementById("result-list");

    for (const result of search_response) {
        text_sections = [];
        
        // get the text sections into a list
        for (let i = 0; i < result.text_sections.length; i++) {
            idx = result.text_sections[i];
            text_sections.push(result.page_data.text_sections[idx]);
        }

        // make the result element
        resultList.appendChild(
            makeTextSearchResult(
                result.thumbnails[0],
                result.page_data.page_url,
                text_sections
            )
        )
    }
}

function showQueryImage(file) {

    console.log("showing query image");

    // clear out the former query image
    document.getElementById("query-image-container").innerHTML = "";

    const reader = new FileReader();

    reader.onload = function(e) {
        const imageUrl = e.target.result;
        const img = document.createElement('img');

        img.src = imageUrl;

        img.setAttribute("class", "query-image");

        document.getElementById('query-image-container').appendChild(img);

    }

    reader.readAsDataURL(file);

}