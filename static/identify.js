function imageDragOverHandler(e) {
    document.getElementById("image-drop-text").innerHTML = "Lets go!";
    e.preventDefault()
}

function imageDropHandler(e) {
    console.log("image file dropped");

    e.preventDefault();
    document.getElementById("image-input").files = e.dataTransfer.files;

    classifyImage({
        target: document.getElementById("image-input")
    });
}

function classifyImage(e) {
    var inputElement = e.target;

    // Check if a file is selected
    if (inputElement.files.length > 0) {
        // Get the selected file
        var file = inputElement.files[0];

        // Create a FormData object
        var formData = new FormData();
        // Append the file to the FormData object
        formData.append('file', file);

        let apiEndpoint = "/identify_species";

        // Example: Sending image data to the API using fetch
        fetch(apiEndpoint, {
            method: 'POST',
            body: formData,
        }).then(response => response.json())
        .then(data => {
            // Handle the API response
            console.log("recieved search api response: ");
            console.log(data);
            showResults(data.classification_result)
        })
    } else {
        alert("Please select an image file.");
    }
}


function showResults(classification_result) {

    // clear the prior results
    document.getElementById("result-list").innerHTML = "";

    let resultList = document.getElementById("result-list");
    for (const resultObject of classification_result) {
        let resultDiv = document.createElement("div");
        resultDiv.setAttribute("class", "classification-result-div");
        let formattedScientificName = resultObject.class_name.replace(" ", "_");
        console.log(formattedScientificName);

        let apiRoute = `https://en.wikipedia.org/api/rest_v1/page/summary/${formattedScientificName}`;
        fetch(apiRoute).then(res => res.json()).then(data => {
            console.log(data);

            // if the wikipedia API returned helpful information, then update the page with the new information
            document.getElementById(`${formattedScientificName}-img`).src = data.thumbnail.source;
            document.getElementById(`${formattedScientificName}-text`).innerHTML = data.extract_html;
        });

        // add a thumbnail for the result that can be accessed if if the wikipedia API provides one
        let resultImage = document.createElement("img");
        resultImage.setAttribute("id", `${formattedScientificName}-img`)
        resultImage.setAttribute("class", "result-image");
        resultDiv.appendChild(resultImage)

        // add a title to the result
        let resultTitle = document.createElement("h1");
        resultTitle.innerHTML = resultObject.class_name

        // preview text to get info about the species that can be accessed by the wikipedia API
        let resultText = document.createElement("p");
        resultText.setAttribute("class", "result-text");
        resultText.setAttribute("id", `${formattedScientificName}-text`)
        resultDiv.appendChild(resultText);


        resultList.appendChild(resultDiv);
    }
}

document.getElementById("image-input").addEventListener("change", classifyImage)
