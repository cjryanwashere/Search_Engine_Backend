import time
import numpy as np
from PIL import Image
import io
import json
import requests
from fastapi import FastAPI, File, UploadFile, Response
from fastapi.responses import JSONResponse, HTMLResponse, PlainTextResponse, FileResponse
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel
import sys


sys.path.append("/code/search_engine")
import search
import naturify_client

# only the things for a naturify API
nat_only = False


if not nat_only:
    search_client = search.SearchEngineClient(
        vector_index_path = "/home/cameron/Search_Engine/index_v1/vector_index",
        page_index_path = "/home/cameron/Search_Engine/index_v1/page_index"
    )

naturify_classification_client = naturify_client.NaturifyClient()

app = FastAPI()




app.mount("/static", StaticFiles(directory="/code/static", html=True), name="static")



# host the browser search page
def search_page():
    with open("/code/static/index.html","r") as f:
        html =  f.read()
        return html
@app.get("/",response_class=HTMLResponse)
async def main_page():
    return search_page()


# search a text query
class TextSearchRequest(BaseModel):
    query: str
@app.post("/search_text")
async def search_text(text_search_request: TextSearchRequest):
    query = text_search_request.query
    results = search_client.search_text(query)
    return JSONResponse({
        "search_result" : results
    })

#class PageQARequest(BaseModel):
#    question: str
#    page_url: str
#@app.post("/page_qa")
#async def page_qa(request: PageQARequest):
#    return JSONResponse(
#        search_client.page_qa(request.page_url, request.question)
#    )

# search an image 
@app.post("/search_image")
async def search_image(file: UploadFile = File(...)):
    # read the image data from the HTTP request and open it
    image = await file.read()
    pil_image = Image.open(io.BytesIO(image))
    results = search_client.search_image(pil_image)
    results = [result.dict() for result in results]
    return JSONResponse({
        "search_result" : results
    })

@app.post("/identify_species")
async def identify_species(file: UploadFile = File(...)):
    # get the bytes for the image
    image_bytes = await file.read()
    # make a call to the gRPC service that hosts the model
    classification_result = naturify_classification_client.classify_image(image_bytes=image_bytes)
    return JSONResponse({
        "classification_result" : classification_result
    })

@app.get("/ads.txt", response_class = FileResponse)
async def ads_txt():
    return "/code/static/ads.txt"
@app.get("/sw.js", response_class=FileResponse)
async def sw_js():
    return "/code/static/sw.js"

    
if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, host='0.0.0.0', port=8000)
